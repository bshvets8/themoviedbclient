package org.bitbucket.bshvets8.themoviedbclient.ui.moviedetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.Movie
import org.bitbucket.bshvets8.themoviedbclient.domain.MoviesUseCase
import org.bitbucket.bshvets8.themoviedbclient.util.ServiceLocator

/**
 * Created by bohdan.shvets on 04.06.2020
 */
class MovieDetailsViewModel : ViewModel() {

	private val _movie: MutableLiveData<Movie?> = MutableLiveData()
	val movie: LiveData<Movie?> = _movie

	private val moviesUseCase: MoviesUseCase = ServiceLocator.defaultInstance.getMoviesUseCase()

	fun initializeMovie(id: Int) {
		viewModelScope.launch {
			_movie.value = moviesUseCase.getMovie(id)
		}
	}
}
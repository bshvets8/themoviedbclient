package org.bitbucket.bshvets8.themoviedbclient.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.bitbucket.bshvets8.themoviedbclient.domain.LoginUseCase
import org.bitbucket.bshvets8.themoviedbclient.util.ServiceLocator

/**
 * Created by bohdan.shvets on 20.04.2020
 */
class MainViewModel : ViewModel() {

	private val _showMovieId: MutableLiveData<Int> = MutableLiveData()
	val showMovieId: LiveData<Int> = _showMovieId

	private val loginUseCase: LoginUseCase = ServiceLocator.defaultInstance.getLoginUseCase()

	fun isLoggedIn(): Boolean {
		return loginUseCase.isLoggedIn()
	}

	fun showMovie(id: Int) {
		_showMovieId.value = id
	}
}
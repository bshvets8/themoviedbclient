package org.bitbucket.bshvets8.themoviedbclient.ui.popular

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.bitbucket.bshvets8.themoviedbclient.R
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.Movie

class PopularMoviesAdapter : RecyclerView.Adapter<PopularMovieViewHolder>() {

	private var popularMovies: List<Movie> = listOf()
	var lastItemBoundCallback: (() -> Unit)? = null
	var showMovieCallback: ((Int) -> Unit)? = null

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularMovieViewHolder {
		return PopularMovieViewHolder(
			LayoutInflater.from(parent.context)
				.inflate(R.layout.list_item_popular_movie, parent, false)
		)
	}

	override fun getItemCount(): Int = popularMovies.size

	override fun onBindViewHolder(holder: PopularMovieViewHolder, position: Int) {
		val popularMovie = popularMovies[position]
		holder.setDescription("${popularMovie.title} (${popularMovie.releaseDate})")
		holder.setImagePath(popularMovie.posterPath ?: "")
		holder.setRatingPercentage((popularMovie.voteAverage * 10).toInt())
		holder.action = { showMovieCallback?.invoke(popularMovie.id) }

		if (position == itemCount - 1)
			lastItemBoundCallback?.invoke()
	}

	fun setMovies(movies: List<Movie>) {
		val oldItemsCount = popularMovies.count()
		popularMovies = movies
		notifyItemRangeInserted(oldItemsCount, movies.size - oldItemsCount)
	}
}
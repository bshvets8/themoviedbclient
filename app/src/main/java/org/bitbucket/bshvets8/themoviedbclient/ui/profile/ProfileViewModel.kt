package org.bitbucket.bshvets8.themoviedbclient.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.profile.User
import org.bitbucket.bshvets8.themoviedbclient.util.ServiceLocator

/**
 * Created by bohdan.shvets on 26.04.2020
 */
class ProfileViewModel : ViewModel() {

	private val profileUseCase = ServiceLocator.defaultInstance.getProfileUseCase()
	private val loginUseCase = ServiceLocator.defaultInstance.getLoginUseCase()

	private val _profileDetails: MutableLiveData<User> = MutableLiveData()
	val profileDetails: LiveData<User> = _profileDetails

	fun initProfileDetails() {
		viewModelScope.launch {
			val profileDetails = profileUseCase.getProfileDetails()
			_profileDetails.value = profileDetails
		}
	}

	fun logOut() {
		loginUseCase.logOut()
	}
}
package org.bitbucket.bshvets8.themoviedbclient.ui.popular

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_popular_movies.*
import org.bitbucket.bshvets8.themoviedbclient.R
import org.bitbucket.bshvets8.themoviedbclient.ui.main.MainViewModel

class PopularMoviesFragment : Fragment() {

	private val popularMoviesViewModel: PopularMoviesViewModel by viewModels()
	private lateinit var mainViewModel: MainViewModel
	private val adapter = PopularMoviesAdapter()

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		return inflater.inflate(R.layout.fragment_popular_movies, container, false)
	}

	override fun onAttach(context: Context) {
		super.onAttach(context)
		activity?.let { mainViewModel = ViewModelProvider(it).get(MainViewModel::class.java) }
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		tvPopularMovies.layoutManager = LinearLayoutManager(context)
		tvPopularMovies.adapter = adapter

		popularMoviesViewModel.popularMovies.observe(viewLifecycleOwner, Observer {
			adapter.setMovies(it)
		})

		popularMoviesViewModel.initPopularMovies()

		adapter.lastItemBoundCallback = {
			popularMoviesViewModel.loadMoreMovies()
		}

		adapter.showMovieCallback = mainViewModel::showMovie
	}
}
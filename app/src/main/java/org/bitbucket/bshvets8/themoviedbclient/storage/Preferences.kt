package org.bitbucket.bshvets8.themoviedbclient.storage

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

/**
 * Created by bohdan.shvets on 20.04.2020
 */

class Preferences(
	application: Application
) {

	private val _preferencesName = "org.bitbucket.bshvets8.themoviedbclient.Preferences"
	private val _sessionIdKey = "org.bitbucket.bshvets8.themoviedbclient.SESSION_ID"

	private val sharedPreferences: SharedPreferences =
		application.getSharedPreferences(_preferencesName, Context.MODE_PRIVATE)

	var sessionId: String
		get() = sharedPreferences.getString(_sessionIdKey, "") ?: ""
		set(value) = sharedPreferences.edit { putString(_sessionIdKey, value) }
}
package org.bitbucket.bshvets8.themoviedbclient.data.model.network.profile

data class Avatar(
    val gravatar: Gravatar
)
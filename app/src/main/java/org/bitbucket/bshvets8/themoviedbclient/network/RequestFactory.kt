package org.bitbucket.bshvets8.themoviedbclient.network

import org.bitbucket.bshvets8.themoviedbclient.data.model.network.PopularMovieResponseBody
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.authentication.AuthenticationRequestBody
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.authentication.AuthenticationResponseBody
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.profile.User
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.requesttoken.RequestTokenResponseBody
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.session.SessionRequestBody
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.session.SessionResponseBody

/**
 * Created by bohdan.shvets on 12.04.2020
 */
class RequestFactory(
	private val service: TheMovieDatabaseService
) {

	suspend fun createRequestToken(): RequestTokenResponseBody {
		return service.createRequestToken(TheMovieDatabaseService.apiKey)
	}

	suspend fun authenticate(authenticationRequestBody: AuthenticationRequestBody): AuthenticationResponseBody {
		return service.authenticate(TheMovieDatabaseService.apiKey, authenticationRequestBody)
	}

	suspend fun createSession(sessionRequestBody: SessionRequestBody): SessionResponseBody {
		return service.createSession(TheMovieDatabaseService.apiKey, sessionRequestBody)
	}

	suspend fun getPopularMovies(page: Int): PopularMovieResponseBody {
		return service.getPopularMovies(TheMovieDatabaseService.apiKey, page)
	}

	suspend fun getProfileDetails(sessionId: String): User {
		return service.getProfileDetails(TheMovieDatabaseService.apiKey, sessionId)
	}
}
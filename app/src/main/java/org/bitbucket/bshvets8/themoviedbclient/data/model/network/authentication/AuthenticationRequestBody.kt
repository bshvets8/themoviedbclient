package org.bitbucket.bshvets8.themoviedbclient.data.model.network.authentication

import com.google.gson.annotations.SerializedName

/**
 * Created by bohdan.shvets on 05.04.2020
 */
data class AuthenticationRequestBody(
	@SerializedName("username") val username: String,
	@SerializedName("password") val password: String,
	@SerializedName("request_token") val requestToken: String
)
package org.bitbucket.bshvets8.themoviedbclient.storage.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Query
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.profile.User

@Dao
interface UserDao {

	@Query("SELECT * FROM user")
	suspend fun getUser(): User

	@Query("DELETE FROM user")
	suspend fun clear()
}
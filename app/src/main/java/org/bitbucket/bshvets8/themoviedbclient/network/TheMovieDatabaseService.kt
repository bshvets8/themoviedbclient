package org.bitbucket.bshvets8.themoviedbclient.network

import org.bitbucket.bshvets8.themoviedbclient.data.model.network.PopularMovieResponseBody
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.authentication.AuthenticationRequestBody
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.authentication.AuthenticationResponseBody
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.profile.User
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.requesttoken.RequestTokenResponseBody
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.session.SessionRequestBody
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.session.SessionResponseBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by bohdan.shvets on 3/30/20
 */
const val apiKeyParameterName = "api_key"

interface TheMovieDatabaseService {

	companion object {

		val avatarBasePath = "https://www.gravatar.com/avatar/"
		val originalImageSizePath = "original"
		val thumbnailImagePath = "w500"
		val imagesBaseUrl = "https://image.tmdb.org/t/p/"
		val baseUrl = "https://api.themoviedb.org"
		val apiKey = "62836458f9e2248d8989a165ef25a726"

		val defaultInstance: TheMovieDatabaseService by lazy {
			val retrofit = Retrofit.Builder()
				.baseUrl(baseUrl)
				.addConverterFactory(GsonConverterFactory.create())
				.build()

			return@lazy retrofit.create(TheMovieDatabaseService::class.java)
		}
	}

	@GET("/3/authentication/token/new")
	suspend fun createRequestToken(@Query(apiKeyParameterName) apiKey: String): RequestTokenResponseBody

	@POST("/3/authentication/token/validate_with_login")
	suspend fun authenticate(
		@Query(apiKeyParameterName) apiKey: String,
		@Body authenticationRequestBody: AuthenticationRequestBody
	): AuthenticationResponseBody

	@POST("/3/authentication/session/new")
	suspend fun createSession(
		@Query(apiKeyParameterName) apiKey: String,
		@Body sessionRequestBody: SessionRequestBody
	): SessionResponseBody

	@POST("/3/movie/popular")
	suspend fun getPopularMovies(
		@Query(apiKeyParameterName) apiKey: String,
		@Query("page") page: Int
	): PopularMovieResponseBody

	@GET("/3/account")
	suspend fun getProfileDetails(
		@Query(apiKeyParameterName) apiKey: String,
		@Query("session_id") sessionId: String
	): User
}
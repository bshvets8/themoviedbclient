package org.bitbucket.bshvets8.themoviedbclient.data.model.network.profile

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "user")
data class User @JvmOverloads constructor(
	@SerializedName("id") @PrimaryKey var id: Int,
	@SerializedName("avatar") @Ignore var avatar: Avatar? = null,
	@SerializedName("include_adult") var includeAdult: Boolean,
	@SerializedName("iso_3166_1") var iso31661: String,
	@SerializedName("iso_639_1") var iso6391: String,
	@SerializedName("name") var name: String,
	@SerializedName("username") var username: String
)
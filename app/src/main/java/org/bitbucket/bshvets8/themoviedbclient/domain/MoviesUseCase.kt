package org.bitbucket.bshvets8.themoviedbclient.domain

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.Movie
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.PopularMovieResponseBody
import org.bitbucket.bshvets8.themoviedbclient.network.RequestFactory
import org.bitbucket.bshvets8.themoviedbclient.storage.database.AppDatabase

class MoviesUseCase(
	private val requestFactory: RequestFactory,
	private val database: AppDatabase
) {

	suspend fun downloadMovies(page: Int): PopularMovieResponseBody = withContext(Dispatchers.IO) {
		return@withContext requestFactory.getPopularMovies(page)
	}

	suspend fun getCachedMovies(): List<Movie> = withContext(Dispatchers.IO) {
		return@withContext database.getMoviesDao().getMovies()
	}

	suspend fun cacheMovies(movies: List<Movie>) = withContext(Dispatchers.IO) {
		database.getMoviesDao().insertMovies(movies)
	}

	suspend fun clearCachedMovies() = withContext(Dispatchers.IO) {
		database.getMoviesDao().clear()
	}

	suspend fun getMovie(id: Int): Movie? = withContext(Dispatchers.IO) {
		return@withContext database.getMoviesDao().getMovie(id)
	}
}
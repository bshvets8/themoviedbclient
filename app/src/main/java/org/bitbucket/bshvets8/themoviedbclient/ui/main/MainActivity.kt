package org.bitbucket.bshvets8.themoviedbclient.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*
import org.bitbucket.bshvets8.themoviedbclient.R
import org.bitbucket.bshvets8.themoviedbclient.ui.login.LoginActivity
import org.bitbucket.bshvets8.themoviedbclient.ui.moviedetails.MovieDetailsActivity
import org.bitbucket.bshvets8.themoviedbclient.ui.popular.PopularMoviesFragment
import org.bitbucket.bshvets8.themoviedbclient.ui.profile.ProfileFragment

/**
 * Created by bohdan.shvets on 20.04.2020
 */
class MainActivity : AppCompatActivity() {

	private lateinit var viewModel: MainViewModel

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

		if (!viewModel.isLoggedIn()) {
			startActivity(Intent(this, LoginActivity::class.java))
			finish()
			return
		}

		setContentView(R.layout.activity_main)

		viewModel.showMovieId.observe(this, Observer { it?.let(this::showMovie) })

		bottomNavigationView.setOnNavigationItemSelectedListener {
			return@setOnNavigationItemSelectedListener when (it.itemId) {
				R.id.nav_popular -> {
					showFragment(PopularMoviesFragment())
					true
				}

				R.id.nav_profile -> {
					showFragment(ProfileFragment())
					true
				}

				else -> false
			}
		}

		bottomNavigationView.selectedItemId = R.id.nav_popular
	}

	private fun showFragment(fragment: Fragment) {
		supportFragmentManager.beginTransaction()
			.replace(R.id.flFragmentContainer, fragment, "")
			.commit()
	}

	private fun showMovie(id: Int) {
		val intent = Intent(this, MovieDetailsActivity::class.java)
		intent.putExtra(MovieDetailsActivity.EXTRA_MOVIE_ID, id)
		startActivity(intent)
	}
}
package org.bitbucket.bshvets8.themoviedbclient.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.bitbucket.bshvets8.themoviedbclient.domain.LoginUseCase
import org.bitbucket.bshvets8.themoviedbclient.util.ServiceLocator
import java.lang.Exception

/**
 * Created by bohdan.shvets on 05.04.2020
 */
class LoginViewModel : ViewModel() {

	private val loginUseCase: LoginUseCase = ServiceLocator.defaultInstance.getLoginUseCase()

	private val _loginResult: MutableLiveData<Boolean> = MutableLiveData()
	val loginResult: LiveData<Boolean> = _loginResult

	private val _loginErrorMessage: MutableLiveData<String> = MutableLiveData()
	val loginErrorMessage: LiveData<String> = _loginErrorMessage

	fun login(userName: String, password: String) {
		viewModelScope.launch {
			try {
				loginUseCase.performLogin(userName, password).let {
					_loginResult.value = it
				}
			} catch (e: Exception) {
				_loginResult.value = false
				_loginErrorMessage.value = e.localizedMessage
			}
		}
	}
}
package org.bitbucket.bshvets8.themoviedbclient.data.model.network.session

import com.google.gson.annotations.SerializedName

/**
 * Created by bohdan.shvets on 05.04.2020
 */
data class SessionRequestBody(
	@SerializedName("request_token") val requestToken: String
) 
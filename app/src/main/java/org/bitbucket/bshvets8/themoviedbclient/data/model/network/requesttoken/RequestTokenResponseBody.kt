package org.bitbucket.bshvets8.themoviedbclient.data.model.network.requesttoken

import com.google.gson.annotations.SerializedName

/**
 * Created by bohdan.shvets on 05.04.2020
 */
data class RequestTokenResponseBody(
	@SerializedName("request_token") val requestToken: String,
	@SerializedName("expires_at") val expiresAt: String,
	@SerializedName("success") val success: Boolean
) 
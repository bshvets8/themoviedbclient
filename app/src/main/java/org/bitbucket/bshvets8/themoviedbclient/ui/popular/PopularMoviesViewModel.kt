package org.bitbucket.bshvets8.themoviedbclient.ui.popular

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.Movie
import org.bitbucket.bshvets8.themoviedbclient.domain.MoviesUseCase
import org.bitbucket.bshvets8.themoviedbclient.util.ServiceLocator

class PopularMoviesViewModel : ViewModel() {

	private val moviesUseCase: MoviesUseCase = ServiceLocator.defaultInstance.getMoviesUseCase()

	private val _popularMovies: MutableLiveData<List<Movie>> = MutableLiveData()
	val popularMovies: LiveData<List<Movie>> = _popularMovies

	private var currentPage = 0
	private var totalPages = 0

	fun initPopularMovies() {
		viewModelScope.launch {
			currentPage = 1
			try {
				val result = moviesUseCase.downloadMovies(currentPage)

				val movies = result.results
				moviesUseCase.clearCachedMovies()
				moviesUseCase.cacheMovies(movies)
				_popularMovies.value = movies

				totalPages = result.totalPages
			} catch (e: Exception) {
				_popularMovies.value = moviesUseCase.getCachedMovies()
				totalPages = 1

				registerNetworkChangedReceiver()
			}
		}
	}

	fun loadMoreMovies() {
		if (!hasMoreMovies())
			return

		viewModelScope.launch {
			currentPage += 1

			try {
				val result = moviesUseCase.downloadMovies(currentPage)
				moviesUseCase.cacheMovies(result.results)
				val currentMovies = _popularMovies.value ?: mutableListOf()
				_popularMovies.value = currentMovies + result.results
				totalPages = result.totalPages
			} catch (e: java.lang.Exception) {
				totalPages = currentPage
			}
		}
	}

	private fun hasMoreMovies(): Boolean {
		return currentPage < totalPages
	}

	private fun registerNetworkChangedReceiver() {
		val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
		ServiceLocator.defaultInstance.getApplicationContext().registerReceiver(networkChangedReceiver, intentFilter)
	}

	private val networkChangedReceiver = object : BroadcastReceiver() {
		override fun onReceive(context: Context?, intent: Intent?) {
			initPopularMovies()
			context?.unregisterReceiver(this)
		}
	}
}
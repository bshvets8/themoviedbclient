package org.bitbucket.bshvets8.themoviedbclient.data.model.network.profile

data class Gravatar(
    val hash: String
)
package org.bitbucket.bshvets8.themoviedbclient.storage.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.Movie

@Dao
interface MoviesDao {

	@Query("SELECT * FROM movies")
	suspend fun getMovies(): List<Movie>

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	suspend fun insertMovies(movies: List<Movie>)

	@Query("DELETE FROM movies")
	suspend fun clear()

	@Query("SELECT * FROM movies WHERE id = :id")
	suspend fun getMovie(id: Int): Movie?
}
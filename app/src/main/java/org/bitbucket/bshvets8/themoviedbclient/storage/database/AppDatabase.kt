package org.bitbucket.bshvets8.themoviedbclient.storage.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.Movie
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.profile.User

@Database(entities = [Movie::class, User::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

	abstract fun getMoviesDao(): MoviesDao

	abstract fun getUserDao(): UserDao

	companion object {

		private val databaseName = "app_database"

		fun create(context: Context): AppDatabase {
			return Room.databaseBuilder(context, AppDatabase::class.java, databaseName).build()
		}
	}
}
package org.bitbucket.bshvets8.themoviedbclient.util

import android.app.Application
import org.bitbucket.bshvets8.themoviedbclient.domain.LoginUseCase
import org.bitbucket.bshvets8.themoviedbclient.domain.MoviesUseCase
import org.bitbucket.bshvets8.themoviedbclient.domain.ProfileUseCase
import org.bitbucket.bshvets8.themoviedbclient.network.RequestFactory
import org.bitbucket.bshvets8.themoviedbclient.network.TheMovieDatabaseService
import org.bitbucket.bshvets8.themoviedbclient.storage.Preferences
import org.bitbucket.bshvets8.themoviedbclient.storage.database.AppDatabase

/**
 * Created by bohdan.shvets on 3/28/20
 */
interface ServiceLocator {

	fun getTheMovieDatabaseService(): TheMovieDatabaseService

	fun getRequestFactory(): RequestFactory

	fun registerApplicationContext(application: Application)

	fun getApplicationContext(): Application

	fun getPreferences(): Preferences

	fun getLoginUseCase(): LoginUseCase

	fun getMoviesUseCase(): MoviesUseCase

	fun getProfileUseCase(): ProfileUseCase

	fun getDatabase(): AppDatabase

	companion object {

		val defaultInstance = object : ServiceLocator {

			private lateinit var application: Application

			private val appDatabase: AppDatabase by lazy {
				return@lazy AppDatabase.create(getApplicationContext())
			}

			override fun getTheMovieDatabaseService() = TheMovieDatabaseService.defaultInstance

			override fun getRequestFactory(): RequestFactory =
				RequestFactory(getTheMovieDatabaseService())

			override fun registerApplicationContext(application: Application) {
				this.application = application
			}

			override fun getApplicationContext(): Application = application

			override fun getPreferences(): Preferences = Preferences(application)

			override fun getLoginUseCase(): LoginUseCase =
				LoginUseCase(getRequestFactory(), getPreferences())

			override fun getMoviesUseCase(): MoviesUseCase = MoviesUseCase(getRequestFactory(), getDatabase())

			override fun getProfileUseCase(): ProfileUseCase =
				ProfileUseCase(getPreferences(), getRequestFactory())

			override fun getDatabase(): AppDatabase = appDatabase
		}
	}
}
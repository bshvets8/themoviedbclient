package org.bitbucket.bshvets8.themoviedbclient

import android.app.Application
import org.bitbucket.bshvets8.themoviedbclient.util.ServiceLocator

/**
 * Created by bohdan.shvets on 20.04.2020
 */

class TheMovieDBClientApp : Application() {

	override fun onCreate() {
		super.onCreate()

		ServiceLocator.defaultInstance.registerApplicationContext(this)
	}
}
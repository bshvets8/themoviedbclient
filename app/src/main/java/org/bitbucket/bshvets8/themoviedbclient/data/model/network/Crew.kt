package org.bitbucket.bshvets8.themoviedbclient.data.model.network

data class Crew(
    val credit_id: String,
    val department: String,
    val gender: Int,
    val id: Int,
    val job: String,
    val name: String,
    val profile_path: String
)
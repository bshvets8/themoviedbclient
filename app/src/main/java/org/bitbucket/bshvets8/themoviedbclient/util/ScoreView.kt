package org.bitbucket.bshvets8.themoviedbclient.util

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import org.bitbucket.bshvets8.themoviedbclient.R
import kotlin.math.min

class ScoreView @JvmOverloads constructor(
	context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

	private var _percentage: Int = 0
	var percentage: Int
		get() = _percentage
		set(value) {
			_percentage = value
			invalidate()
		}

	private val backgroundPint = Paint().apply {
		color = ContextCompat.getColor(context, R.color.colorPrimary)
		isAntiAlias = true
	}

	private val arcPaint = Paint().apply {
		color = ContextCompat.getColor(context, R.color.colorAccent)
		strokeCap = Paint.Cap.ROUND
		style = Paint.Style.STROKE
		isAntiAlias = true
	}

	private val textPaint = Paint().apply {
		color = Color.WHITE
		isAntiAlias = true
	}

	private val arcPath = Path()
	private val textBounds = Rect()

	override fun onDraw(canvas: Canvas?) {
		if (canvas == null)
			return

		val smallestSize = min(height, width).toFloat()

		val radius = smallestSize / 2f
		val arcPadding = smallestSize / 10f

		arcPaint.strokeWidth = smallestSize / 20f

		canvas.drawCircle(radius, radius, radius, backgroundPint)
		arcPath.addArc(
			arcPadding,
			arcPadding,
			smallestSize - arcPadding,
			smallestSize - arcPadding,
			270f,
			_percentage.toFloat() * 360f / 100f
		)
		canvas.drawPath(arcPath, arcPaint)
		arcPath.reset()

		val textSize = smallestSize / 3f
		val text = "$_percentage%"
		textPaint.textSize = textSize
		textPaint.getTextBounds(text, 0, text.length, textBounds)
		canvas.drawText(
			text,
			(smallestSize - textBounds.width()) / 2f,
			smallestSize - (smallestSize - textBounds.height()) / 2f,
			textPaint
		)
	}
}
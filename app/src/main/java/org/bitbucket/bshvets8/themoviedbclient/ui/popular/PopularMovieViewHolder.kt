package org.bitbucket.bshvets8.themoviedbclient.ui.popular

import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.list_item_popular_movie.view.*
import org.bitbucket.bshvets8.themoviedbclient.network.TheMovieDatabaseService
import org.bitbucket.bshvets8.themoviedbclient.util.ScoreView

class PopularMovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

	private val ivMoviePoster: ImageView = itemView.ivMoviePoster
	private val tvDescription: TextView = itemView.tvDescription
	private val scoreView: ScoreView = itemView.scoreView
	private val cardView: CardView = itemView.cardView

	var action: (() -> Unit)? = null

	init {
		cardView.setOnClickListener {
			action?.invoke()
		}
	}

	fun setImagePath(path: String?) {
		if (path == null) {
			ivMoviePoster.setImageDrawable(null)
			return
		}

		val uri = Uri.Builder()
			.encodedPath(TheMovieDatabaseService.imagesBaseUrl)
			.appendPath(TheMovieDatabaseService.thumbnailImagePath)
			.appendPath(path.replace("/", ""))
			.appendQueryParameter("apiKey", TheMovieDatabaseService.apiKey)
			.build()

		Glide.with(itemView.context)
			.load(uri.toString())
			.into(ivMoviePoster)
	}

	fun setDescription(description: String) {
		tvDescription.text = description
	}

	fun setRatingPercentage(ratingPercentage: Int) {
		scoreView.percentage = ratingPercentage
	}
}
package org.bitbucket.bshvets8.themoviedbclient.domain

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.profile.User
import org.bitbucket.bshvets8.themoviedbclient.network.RequestFactory
import org.bitbucket.bshvets8.themoviedbclient.storage.Preferences

/**
 * Created by bohdan.shvets on 03.05.2020
 */
class ProfileUseCase(
	private val preferences: Preferences,
	private val requestFactory: RequestFactory
) {

	suspend fun getProfileDetails(): User = withContext(Dispatchers.IO) {
		return@withContext requestFactory.getProfileDetails(preferences.sessionId)
	}
}
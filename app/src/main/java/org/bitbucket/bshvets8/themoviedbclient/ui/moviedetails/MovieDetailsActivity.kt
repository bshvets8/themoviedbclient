package org.bitbucket.bshvets8.themoviedbclient.ui.moviedetails

import android.net.Uri
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_movie_details.*
import org.bitbucket.bshvets8.themoviedbclient.R
import org.bitbucket.bshvets8.themoviedbclient.network.TheMovieDatabaseService

/**
 * Created by bohdan.shvets on 04.06.2020
 */

class MovieDetailsActivity : AppCompatActivity() {

	companion object {
		const val EXTRA_MOVIE_ID = "EXTRA_MOVIE_ID"
	}

	private val viewModel: MovieDetailsViewModel by viewModels()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		setContentView(R.layout.activity_movie_details)

		viewModel.movie.observe(this, Observer { movie ->
			movie?.apply {
				showPoster(movie.posterPath ?: "")
				setDescription(movie.title)
			}
		})

		viewModel.initializeMovie(intent.getIntExtra(EXTRA_MOVIE_ID, 0))
	}

	private fun showPoster(path: String) {
		val uri = Uri.Builder()
			.encodedPath(TheMovieDatabaseService.imagesBaseUrl)
			.appendPath(TheMovieDatabaseService.originalImageSizePath)
			.appendPath(path.replace("/", ""))
			.appendQueryParameter("apiKey", TheMovieDatabaseService.apiKey)
			.build()

		Glide.with(ivMoviePoster.context)
			.load(uri.toString())
			.into(ivMoviePoster)
	}

	private fun setDescription(description: String) {
		tvDescription.text = description
	}
}
package org.bitbucket.bshvets8.themoviedbclient.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_profile.*
import org.bitbucket.bshvets8.themoviedbclient.R
import org.bitbucket.bshvets8.themoviedbclient.network.TheMovieDatabaseService
import org.bitbucket.bshvets8.themoviedbclient.ui.login.LoginActivity

/**
 * Created by bohdan.shvets on 26.04.2020
 */
class ProfileFragment : Fragment() {

	private val viewModel: ProfileViewModel by viewModels()

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		return inflater.inflate(R.layout.fragment_profile, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		viewModel.profileDetails.observe(viewLifecycleOwner, Observer {
			tvUserName.text = it.name

			it.avatar?.let {
				Glide.with(this)
					.load(TheMovieDatabaseService.avatarBasePath + it.gravatar.hash)
					.into(ivProfilePicture)
			}
		})

		btnLogout.setOnClickListener {
			viewModel.logOut()
			startActivity(Intent(context, LoginActivity::class.java))
			activity?.finish()
		}

		viewModel.initProfileDetails()
	}
}
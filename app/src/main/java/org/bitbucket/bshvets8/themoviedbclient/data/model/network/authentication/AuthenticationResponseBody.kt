package org.bitbucket.bshvets8.themoviedbclient.data.model.network.authentication

import com.google.gson.annotations.SerializedName

/**
 * Created by bohdan.shvets on 05.04.2020
 */
data class AuthenticationResponseBody(
	@SerializedName("expires_at") val expiresAt: String,
	@SerializedName("request_token") val requestToken: String,
	@SerializedName("success") val success: Boolean
)
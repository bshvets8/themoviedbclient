package org.bitbucket.bshvets8.themoviedbclient.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_login.*
import org.bitbucket.bshvets8.themoviedbclient.R
import org.bitbucket.bshvets8.themoviedbclient.ui.main.MainActivity

/**
 * Created by bohdan.shvets on 05.04.2020
 */
class LoginActivity : AppCompatActivity() {

	private val viewModel: LoginViewModel by viewModels()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_login)

		viewModel.loginResult.observe(this, Observer {
			setViewEnabled(true)
			if (it) {
				startActivity(Intent(this, MainActivity::class.java))
				finish()
				return@Observer
			}

			etUserName.error = "Error"
		})

		viewModel.loginErrorMessage.observe(this, Observer {
			if (it == null || it.isEmpty()) {
				setErrorMessageVisible(false)
				return@Observer
			}

			setErrorMessageVisible(true)
			tvErrorMessage.text = it
		})

		btnLogin.setOnClickListener {
			setViewEnabled(false)
			viewModel.login(etUserName.text.toString(), etPassword.text.toString())
		}

		setErrorMessageVisible(false)
	}

	fun setViewEnabled(isEnabled: Boolean) {
		btnLogin.isEnabled = isEnabled
		etUserName.isEnabled = isEnabled
		etPassword.isEnabled = isEnabled
		tvErrorMessage.isEnabled = isEnabled
	}

	private fun setErrorMessageVisible(isVisible: Boolean) {
		tvErrorMessage.visibility = if (isVisible) View.VISIBLE else View.GONE
	}
}
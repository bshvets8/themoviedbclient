package org.bitbucket.bshvets8.themoviedbclient.data.model.network

data class CreditsResponseBody(
    val cast: List<Cast>,
    val crew: List<Crew>,
    val id: Int
)
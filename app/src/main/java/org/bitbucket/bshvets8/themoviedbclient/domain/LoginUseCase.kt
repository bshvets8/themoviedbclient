package org.bitbucket.bshvets8.themoviedbclient.domain

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.authentication.AuthenticationRequestBody
import org.bitbucket.bshvets8.themoviedbclient.data.model.network.session.SessionRequestBody
import org.bitbucket.bshvets8.themoviedbclient.network.RequestFactory
import org.bitbucket.bshvets8.themoviedbclient.storage.Preferences

/**
 * Created by bohdan.shvets on 05.04.2020
 */
class LoginUseCase(
	private val requestFactory: RequestFactory,
	private val preferences: Preferences
) {

	suspend fun performLogin(userName: String, password: String): Boolean =
		withContext(Dispatchers.IO) {
			val token = requestFactory.createRequestToken().requestToken
			val authResult = requestFactory.authenticate(
				AuthenticationRequestBody(userName, password, token)
			)

			if (!authResult.success)
				return@withContext false

			val body = requestFactory.createSession(SessionRequestBody(token))

			preferences.sessionId = body.sessionId

			return@withContext true
		}

	fun logOut() {
		preferences.sessionId = ""
	}

	fun isLoggedIn(): Boolean {
		return preferences.sessionId.isNotEmpty()
	}
}